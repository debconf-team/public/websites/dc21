---
title: DebConf21 Online schedule published
---

The schedule for DebConf21 Online has just been finalized, and is now public.

DebConf21 will run from Tuesday, August 24th, to Saturday, August 28th. Despite
that, we have a schedule full of activities, and those 5 days will be full of
content for the Debian community.

Talks will be presented in two parallel "rooms". "Talks 1" will have talks in
English, while "Talks 2" will host some talks English, but also talks in the
Indian languages (Hindi, Kannada, Malayalam, Marathi, Telugu), organized by the
Debian India community, and well as talks in Portuguese, organized by the
Debian Brazil team.

You can check out the [full schedule](/schedule/), and the full [list of
talks](/talks/). For those interested, we also provide a [mobile-friendly
schedule](/schedule/mobile/).
