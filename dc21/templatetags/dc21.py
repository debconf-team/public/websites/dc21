from django import template


register = template.Library()


@register.filter
def stream(venue):
    if "talks" in venue.name.lower():
        return "main"
    elif "ad-hoc" in venue.name.lower():
        return "adhoc"
    else:
        raise NotImplemented(f"No video stream for {venue.name}")


@register.filter
def channel(venue):
    if "talks" in venue.name.lower():
        return "dc21-talks"
    elif "ad-hoc" in venue.name.lower():
        return "dc21-adhoc"
    else:
        raise NotImplemented(f"No IRC channel for {venue.name}")
