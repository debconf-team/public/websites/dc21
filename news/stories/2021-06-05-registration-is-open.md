---
title: Registration for DebConf21 Online is Open
---

The DebConf team is glad to announce that registration for DebConf21 Online is
now open. To register for DebConf21, please visit our website at

https://debconf21.debconf.org/register

Reminder: Creating an account on the site does not register you for the
conference, there's a conference registration form to complete after
signing in.

Participation in DebConf21 is conditional on your respect of our Code of
Conduct [0]. We require you to read, understand and abide by this code.

[0]: <https://debconf21.debconf.org/about/coc/>

A few notes about the registration process:

- We need to know attendees' locations to better plan the schedule around
  timezones. Please make sure you fill in the "Country I call home" field in
  the registration form accordingly. It's especially important to have this
  data for people who submitted talks, but also for other attendees.

- We are offering limited amounts of financial support for those who
  require it in order to attend. Please refer to the [corresponding
  page](/about/bursaries/) on the website for more information.

Any questions about registration should be addressed to
<registration@debconf.org>.
