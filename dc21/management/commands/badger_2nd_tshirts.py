# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from debconf.tz import aoe_datetime
from register.models import Attendee
from volunteers.models import Volunteer
from wafer.talks.models import ACCEPTED


SUBJECT = "DebConf21 T-Shirts"

TEMPLATE = """\
Hello!

We're contacting you because you either volunteered or gave a talk at DebConf21
Online and didn't get a t-shirt in the first round.  We're organising a small
second round, and would like to know whether or not you'd like a t-shirt.  The
shirt would be provided to you free of charge (aside from any possible
customs/import charges, of course).  If you're interested, please reply with
your name, t-shirt size, and shipping address before December 12th.  If you
don't want a t-shirt, please do let us know explicitly.

Please reply by December 12th.

All the best,

The DebConf Registration/T-shirts Team
"""

class Command(BaseCommand):
    help = 'Send tshirt 2nd-printing emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, user, dry_run):
        name = user.userprofile.display_name()
        to = user.email
        body = TEMPLATE

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        User = get_user_model()
        deadline = aoe_datetime(settings.DEBCONF_CONFIRMATION_DEADLINE)
        for user in User.objects.all():
            if options['username']:
                if options['username'] != user.username:
                    continue
            else:
                if not user.talks.filter(status=ACCEPTED).exists():
                    try:
                        if user.volunteer.tasks.count() < 3:
                            continue
                    except Volunteer.DoesNotExist:
                        continue
                try:
                    if (user.attendee.queues.filter(
                                    queue__name='Swag', timestamp__lt=deadline
                                ).exists()
                            and user.attendee.completed_timestamp <= deadline):
                        continue
                except Attendee.DoesNotExist:
                    pass
            self.badger(user, dry_run)
