---
name: Registration Information
---
# Registration Information

## Registration Fees

As always, basic registration for DebConf is free of charge for
attendees.

If you are attending the conference in a professional capacity, or as a
representative of your company, we ask that you consider registering in
one of our paid categories, to help cover the costs of putting on the
conference and support subsidizing other community members:

**Professional registration**: with a rate of 50 USD for the week.

**Corporate registration**: with a registration fee of 200 USD, this
category is intended for those participating in DebConf as
representatives of companies.

We encourage all attendees to help make DebConf a success by selecting
the registration category appropriate for their situation.
